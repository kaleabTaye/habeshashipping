package com.advancedandroidproject.habeshashipping


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.advancedandroidproject.habeshashipping.data.Driver
import com.advancedandroidproject.habeshashipping.data.Order
import com.advancedandroidproject.habeshashipping.viewmodel.DriverViewModel
import com.advancedandroidproject.habeshashipping.viewmodel.OrderViewModel
import kotlinx.android.synthetic.main.fragment_order_detail.view.*

class OrderDetailFragment : Fragment() {

    private lateinit var orderId: TextView
    private lateinit var orderItem: TextView
    private lateinit var numOfItems: TextView
    private lateinit var vehiclesNeeded: TextView
    private lateinit var orderingCompany: TextView
    private lateinit var orderingStatus: TextView
    private lateinit var assignDriverButton: Button

    private lateinit var orderViewModel: OrderViewModel
    private lateinit var driverViewModel: DriverViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        orderViewModel = ViewModelProviders.of(this).get(OrderViewModel::class.java)
        driverViewModel = ViewModelProviders.of(this).get(DriverViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_order_detail, container, false)
        orderId = view.detail_order_id_tv
        orderItem = view.detail_order_item_tv
        numOfItems = view.detail_num_of_items_tv
        vehiclesNeeded = view.detail_vehicles_needed_tv
        orderingCompany = view.detail_ordering_company_tv
        orderingStatus = view.detail_order_status_tv
        assignDriverButton = view.detail_assign_driver_button

        var itemNeed: String
        val item = arguments?.getString("orderArgs")
        if(item != null) {
            orderViewModel.searchItem(item).observe(this, Observer {
                orderId.text = it.id.toString()
                orderItem.text = it.item
                numOfItems.text = it.numOfItems.toString()
                vehiclesNeeded.text = it.vechilesNeeded.toString()
                //For orderingcompany reserved
                orderingStatus.text = it.itemArrived.toString()
                itemNeed = item
                assignDriverButton.setOnClickListener {
                    val direction = OrderDetailFragmentDirections.assignDriver(item)
                    findNavController().navigate(direction)
                }
            })
        }



        val assignedDriverName = arguments?.getString("availableDriversArgs")

        if(assignedDriverName != null){
            driverViewModel.searchDriver(assignedDriverName).observe(this,Observer{
                val selectedDriver = Driver(it.id,it.name,it.experience,false,it.userName,it.password)
                driverViewModel.updateDriver(selectedDriver)
            })

        }

        return view
    }


}
