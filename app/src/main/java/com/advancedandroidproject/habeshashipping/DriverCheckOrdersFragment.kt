package com.advancedandroidproject.habeshashipping


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.advancedandroidproject.habeshashipping.viewmodel.DriverViewModel
import com.advancedandroidproject.habeshashipping.viewmodel.OrderViewModel
import kotlinx.android.synthetic.main.fragment_driver_check_orders.view.*


class DriverCheckOrdersFragment : Fragment() {

    private lateinit var orderIdTextView: TextView
    private lateinit var orderItemTextView: TextView

    private lateinit var orderViewModel: OrderViewModel
    private lateinit var driverViewModel: DriverViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        orderViewModel = ViewModelProviders.of(this).get(OrderViewModel::class.java)
        driverViewModel = ViewModelProviders.of(this).get(DriverViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view=  inflater.inflate(R.layout.fragment_driver_check_orders, container, false)
        orderIdTextView = view.order_id_tv
        orderItemTextView = view.order_item_tv

        orderViewModel.allOrders.observe(this, Observer {
            var itemName: String
            var itemId: Int
            it.forEach { if(it.inProgress == true){itemName = it.item;itemId = it.id
                orderIdTextView.text = itemId.toString();orderItemTextView.text = itemName} }

        })

        return view
    }


}
