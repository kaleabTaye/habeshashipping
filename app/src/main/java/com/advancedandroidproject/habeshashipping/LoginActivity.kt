package com.advancedandroidproject.habeshashipping

import android.content.Intent
import android.content.res.Configuration
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.advancedandroidproject.habeshashipping.data.Driver
import com.advancedandroidproject.habeshashipping.data.DriverDao
import com.advancedandroidproject.habeshashipping.data.Manager
import com.advancedandroidproject.habeshashipping.data.OrderingCompany
import com.advancedandroidproject.habeshashipping.viewmodel.DriverViewModel
import com.advancedandroidproject.habeshashipping.viewmodel.ManagerViewModel
import com.advancedandroidproject.habeshashipping.viewmodel.OrderingCompanyViewModel
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.login_register_fragment) as NavHostFragment? ?: return
        val navController = host.navController

    }
}

