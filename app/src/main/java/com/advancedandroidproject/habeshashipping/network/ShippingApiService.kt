package com.advancedandroidproject.habeshashipping.network

import androidx.room.Delete
import com.advancedandroidproject.habeshashipping.data.Driver
import com.advancedandroidproject.habeshashipping.data.Manager
import com.advancedandroidproject.habeshashipping.data.Order
import com.advancedandroidproject.habeshashipping.data.OrderingCompany
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

interface ShippingApiService {

    //For the Driver

    @GET("/drivers")
    fun listAllDrivers(): Deferred<Response<List<Driver>>>

    @GET("/drivers/{id}")
    fun getDriverbyId(@Path("id") id: Int): Deferred<Response<Driver>>

    @GET("/drivers")
    fun getDriverbyName(@Query("name") name: String): Deferred<Response<Driver>>

    @POST("/drivers")
    fun registerDriver(@Body driver: Driver): Deferred<Response<Void>>

    @PUT("/drivers/{id}")
    fun updateDriver(@Path("id") id: Int, @Body driver: Driver): Deferred<Response<Void>>

    @DELETE("/drivers/{id}")
    fun deleteDriver(@Path("id") id: Int): Deferred<Response<Void>>

    //For the Manager

    @GET("/managers")
    fun getManager(): Deferred<Response<Manager>>

    @PUT("/managers/{id}")
    fun updateManager(@Path("id") id: Int,@Body manager: Manager): Deferred<Response<Void>>

    //For the Company

    @GET("/ordering-companies/{id}")
    fun getOrderingCompanyById(@Path("id") id: Int): Deferred<Response<OrderingCompany>>

    @GET("/ordering-companies")
    fun getOrderingCompanies(): Deferred<Response<List<OrderingCompany>>>

    @POST("/ordering-companies")
    fun registerOrderingCompany(@Body orderingCompany: OrderingCompany): Deferred<Response<Void>>

    @PUT("/ordering-companies/{id}")
    fun updateOrderingCompany(@Path("id")id: Int, @Body orderingCompany: OrderingCompany): Deferred<Response<Void>>

    //For the Order

    @GET("/orders")
    fun getOrders(): Deferred<Response<List<Order>>>

    @GET("/orders/{id}")
    fun getOrderById(@Path("id")id: Int): Deferred<Response<Order>>

    @POST("/orders")
    fun placeOrder(@Body order: Order): Deferred<Response<Void>>

    @PUT("/orders/{id}")
    fun updateOrder(@Path("id")id: Int, @Body order: Order): Deferred<Response<Void>>

    @DELETE("/orders{id}")
    fun deleteOrder(@Path("id")id: Int): Deferred<Response<Void>>

    companion object {

        private val baseUrl = "http://192.168.43.38:3000/api/"

        fun getInstance(): ShippingApiService {

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            val client = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
                .build()

            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(ShippingApiService::class.java)
        }
    }
}