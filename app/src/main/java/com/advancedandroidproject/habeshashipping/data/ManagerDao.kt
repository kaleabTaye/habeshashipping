package com.advancedandroidproject.habeshashipping.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ManagerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertManager(manager: Manager): Long

    @Query("SELECT * FROM manager WHERE user_name = :userName AND password = :password LIMIT 1")
    fun validateManager(userName:String,password:String): Manager

    @Update
    fun updateManager(manager:Manager):Int
}