package com.advancedandroidproject.habeshashipping.data

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface OrderingCompanyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun registerCompany(orderingCompany: OrderingCompany): Long

    @Update
    fun updateCompany(orderingCompany: OrderingCompany): Int

    @Query("SELECT * FROM ordering_company WHERE user_name = :userName AND password = :password LIMIT 1")
    fun validateOrderingCompany(userName :String,password:String): OrderingCompany
}