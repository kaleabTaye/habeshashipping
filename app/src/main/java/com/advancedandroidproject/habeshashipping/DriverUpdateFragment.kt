package com.advancedandroidproject.habeshashipping


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.advancedandroidproject.habeshashipping.data.Driver
import com.advancedandroidproject.habeshashipping.viewmodel.DriverViewModel
import kotlinx.android.synthetic.main.fragment_driver_update.view.*


class DriverUpdateFragment : Fragment() {


    private lateinit var idEditText: EditText
    private lateinit var nameEditText: EditText
    private lateinit var experienceEditText: EditText
    private lateinit var userNameEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var saveButton: Button

    private lateinit var driverViewModel: DriverViewModel

    private lateinit var currentDriver: Driver
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        driverViewModel = ViewModelProviders.of(this).get(DriverViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_driver_update, container, false)
        idEditText = view.update_id_et
        nameEditText = view.update_name_et
        experienceEditText = view.update_experience_et
        userNameEditText = view.update_user_name_et
        passwordEditText = view.update_password_edit_text_driver
        saveButton = view.update_save_button

        val name = arguments?.getString("driverUpdateArgs") as String
        Toast.makeText(this.context,name, Toast.LENGTH_SHORT).show()

        driverViewModel.searchDriver(name).observe(this,Observer{
            driver -> oldFields(driver)
        })

        saveButton.setOnClickListener {
            val driver = readFields()
            driverViewModel.updateDriver(driver.id,driver)
            clearFields()
        }

        return view
    }

    private fun oldFields(driver: Driver){
        idEditText.setText(driver.id.toString())
        nameEditText.setText(driver.name)
        experienceEditText.setText(driver.experience.toString())
        userNameEditText.setText(driver.userName)
        passwordEditText.setText(driver.password)
    }

    private fun readFields() :Driver{

        currentDriver = Driver(idEditText.text.toString().toInt(),
            nameEditText.text.toString(),
            experienceEditText.text.toString().toInt(),
            false,
            userNameEditText.text.toString(),
            passwordEditText.text.toString()
        )
        return currentDriver
    }
    private fun clearFields(){
        idEditText.setText("")
        nameEditText.setText("")
        experienceEditText.setText("")
        userNameEditText.setText("")
        passwordEditText.setText("")
    }
}


