package com.advancedandroidproject.habeshashipping


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.advancedandroidproject.habeshashipping.adapters.OrderListAdapter
import com.advancedandroidproject.habeshashipping.viewmodel.OrderViewModel
import kotlinx.android.synthetic.main.fragment_order_list.view.*

class OrderListFragment : Fragment() {

    private lateinit var orderList: RecyclerView
    private lateinit var orderViewModel: OrderViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        orderViewModel = ViewModelProviders.of(this).get(OrderViewModel::class.java)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_order_list, container, false)
        orderList = view.order_list
        orderList.layoutManager = LinearLayoutManager(activity)
        val orderAdapter = OrderListAdapter()
        orderList.adapter = orderAdapter
        orderList.setHasFixedSize(true)
        orderViewModel.allOrders.observe(this, Observer{
                orders -> orders?.let{orderAdapter.setOrders(orders)}
        })

        return view
    }


}
