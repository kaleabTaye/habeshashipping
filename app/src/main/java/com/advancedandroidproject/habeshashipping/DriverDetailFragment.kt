package com.advancedandroidproject.habeshashipping


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.advancedandroidproject.habeshashipping.data.Driver
import com.advancedandroidproject.habeshashipping.viewmodel.DriverViewModel
import kotlinx.android.synthetic.main.fragment_driver_detail.view.*


class DriverDetailFragment : Fragment() {

    private lateinit var idTextView: TextView
    private lateinit var nameTextView: TextView
    private lateinit var experienceTextView: TextView
    private lateinit var userNameTextView: TextView
    private lateinit var availableTextView: TextView
    private lateinit var passwordTextView: TextView
    private lateinit var deleteButton: Button
    private lateinit var updateButton: Button

    private lateinit var driverViewModel: DriverViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        driverViewModel = ViewModelProviders.of(this).get(DriverViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_driver_detail, container, false)
        idTextView = view.dt_id_tv
        nameTextView = view.dt_name_tv
        experienceTextView = view.dt_experience_tv
        userNameTextView = view.dt_username_tv
        availableTextView = view.dt_available_tv
        passwordTextView = view.dt_password_tv
        updateButton = view.dt_update_button
        deleteButton = view.dt_delete_button

        val name = arguments?.getString("driverArgs") as String
        driverViewModel.searchDriver(name).observe(this, Observer {
            driver -> updateFields(driver)
        })

        updateButton.setOnClickListener {
            driverViewModel.searchDriver(name).observe(this,Observer{
                val currentDriver = Driver(it.id,it.name,it.experience,it.isAvailable,it.userName,it.password)
                val direction = DriverDetailFragmentDirections.driverUpdate(currentDriver.name)
                findNavController().navigate(direction)
            })

        }
        deleteButton.setOnClickListener {
            driverViewModel.searchDriver(name).observe(this,Observer{
               driver -> driverViewModel.deleteDriver(driver)
                findNavController().navigate(R.id.driverList,null)
            })

        }


        return view
    }
    private fun updateFields(driver: Driver){
        idTextView.setText(driver.id.toString())
        nameTextView.setText(driver.name)
        experienceTextView.setText(driver.experience.toString())
        userNameTextView.setText(driver.userName)
        availableTextView.setText(driver.isAvailable.toString())
        passwordTextView.setText(driver.password)

    }


}
