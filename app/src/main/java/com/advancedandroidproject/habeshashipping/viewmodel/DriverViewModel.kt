package com.advancedandroidproject.habeshashipping.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.advancedandroidproject.habeshashipping.data.Driver
import com.advancedandroidproject.habeshashipping.data.ShippingDatabase
import com.advancedandroidproject.habeshashipping.repository.DriverRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DriverViewModel(application: Application) : AndroidViewModel(application) {

    private val driverRepository: DriverRepository
    val allDrivers: LiveData<List<Driver>>

    init{
        val driverDao = ShippingDatabase.getDatabase(application).driverDao()
        driverRepository = DriverRepository(driverDao)
        GlobalScope.launch{
            driverRepository.getAllDriversFromApi()
        }
        allDrivers = driverRepository.getAllDriversFromRoom()
    }

    fun searchDriver(name: String)= driverRepository.searchDriver(name)

    fun validateDriver(userName: String,password: String)= driverRepository.validateDriver(userName,password)

    fun getAvailableDrivers() = driverRepository.getAvailableDrivers()

    fun registerDriver(driver: Driver) = viewModelScope.launch(Dispatchers.IO){
        driverRepository.registerDriverToApi(driver)
    }
    fun updateDriver(id:Int,driver:Driver) = viewModelScope.launch(Dispatchers.IO){
        driverRepository.updateDriverToApi(id,driver)
    }
    fun deleteDriver(id:Int, driver: Driver) = viewModelScope.launch(Dispatchers.IO){
        driverRepository.deleteDriverFromApi(id,driver)
    }
}