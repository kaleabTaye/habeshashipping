package com.advancedandroidproject.habeshashipping.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.advancedandroidproject.habeshashipping.data.OrderingCompany
import com.advancedandroidproject.habeshashipping.data.ShippingDatabase
import com.advancedandroidproject.habeshashipping.repository.OrderingCompanyRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class OrderingCompanyViewModel(application: Application): AndroidViewModel(application) {

    private val orderingCompanyRepository: OrderingCompanyRepository

    init{
        val orderingCompanyDao = ShippingDatabase.getDatabase(application).orderingCompanyDao()
        orderingCompanyRepository = OrderingCompanyRepository(orderingCompanyDao)
        GlobalScope.launch{
            orderingCompanyRepository.getAllOrderingCompaniesFromApi()
        }
    }

    fun validateOrderingCompany(userName: String,password: String) = orderingCompanyRepository.validateOrderingCompany(userName,password)


    fun registerCompany(orderingCompany: OrderingCompany) = viewModelScope.launch(Dispatchers.IO){
        orderingCompanyRepository.registerOrderingCompanyToApi(orderingCompany)
    }
    fun updateCompany(id:Int, orderingCompany: OrderingCompany) = viewModelScope.launch(Dispatchers.IO){
        orderingCompanyRepository.updateOrderingCompanyToApi(id,orderingCompany)
    }
}