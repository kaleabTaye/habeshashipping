package com.advancedandroidproject.habeshashipping.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.advancedandroidproject.habeshashipping.data.Order
import com.advancedandroidproject.habeshashipping.data.ShippingDatabase
import com.advancedandroidproject.habeshashipping.repository.OrderRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class OrderViewModel(application: Application): AndroidViewModel(application) {

    private val orderRepository: OrderRepository
    val allOrders : LiveData<List<Order>>

    init{
        val orderDao = ShippingDatabase.getDatabase(application).orderDao()
        orderRepository = OrderRepository(orderDao)
        GlobalScope.launch{
            orderRepository.getAllOrdersFromApi()
        }
        allOrders = orderRepository.getAllItems()
    }

    fun searchItem(item: String) = orderRepository.searchItem(item)

    fun placeOrder(order: Order) = viewModelScope.launch(Dispatchers.IO){
        orderRepository.registerOrderToApi(order)
    }

    fun editOrder(order: Order) = viewModelScope.launch(Dispatchers.IO){
        orderRepository.editOrder(order)
    }

    fun deleteOrder(id:Int, order: Order) = viewModelScope.launch(Dispatchers.IO){
        orderRepository.deleteOrderFromApi(id,order)
    }
}