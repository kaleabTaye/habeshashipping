package com.advancedandroidproject.habeshashipping.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.advancedandroidproject.habeshashipping.data.Manager
import com.advancedandroidproject.habeshashipping.data.ShippingDatabase
import com.advancedandroidproject.habeshashipping.repository.ManagerRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ManagerViewModel(application: Application) : AndroidViewModel(application) {

    private val managerRepository: ManagerRepository

    init{
        val managerDao = ShippingDatabase.getDatabase(application).managerDao()
        managerRepository = ManagerRepository(managerDao)
    }

    fun validateManager(userName: String,password: String)= managerRepository.validateManager(userName,password)

    fun insertManager(manager: Manager) = managerRepository.insertManager(manager)

    fun updateManager(id:Int, manager: Manager) = viewModelScope.launch(Dispatchers.IO){
        managerRepository.updateManagerToApi(id,manager)
        managerRepository.updateManager(manager)
    }

}