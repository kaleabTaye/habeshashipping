package com.advancedandroidproject.habeshashipping


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import com.advancedandroidproject.habeshashipping.data.Driver
import com.advancedandroidproject.habeshashipping.viewmodel.DriverViewModel
import kotlinx.android.synthetic.main.fragment_register_driver.view.*


class RegisterDriverFragment : Fragment() {

    private lateinit var idEditText: EditText
    private lateinit var nameEditText: EditText
    private lateinit var experienceEditText: EditText
    private lateinit var userNameEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var saveButton: Button

    private lateinit var driverViewModel: DriverViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        driverViewModel = ViewModelProviders.of(this).get(DriverViewModel::class.java)
    }
    
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view= inflater.inflate(R.layout.fragment_register_driver, container, false)
        idEditText = view.update_id_et
        nameEditText = view.update_name_et
        experienceEditText = view.update_experience_et
        userNameEditText = view.update_user_name_et
        passwordEditText = view.update_password_edit_text_driver
        saveButton = view.update_save_button
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        saveButton.setOnClickListener {
            val driver = Driver(idEditText.text.toString().toInt(),nameEditText.text.toString(),
                    experienceEditText.text.toString().toInt(),true,userNameEditText.text.toString(),passwordEditText.text.toString())
            driverViewModel.registerDriver(driver)

        }
    }

}
