package com.advancedandroidproject.habeshashipping


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import com.advancedandroidproject.habeshashipping.data.Order
import com.advancedandroidproject.habeshashipping.viewmodel.OrderViewModel
import kotlinx.android.synthetic.main.fragment_order.view.*

class OrderFragment : Fragment() {

    private lateinit var idEditText: EditText
    private lateinit var itemEditText: EditText
    private lateinit var noOfItems: EditText
    private lateinit var vehiclesNeeded: EditText
    private lateinit var orderButton: Button

    private lateinit var orderViewModel: OrderViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        orderViewModel = ViewModelProviders.of(this).get(OrderViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_order, container, false)
        idEditText = view.update_id_et
        itemEditText = view.item_et
        noOfItems = view.num_of_items_et
        vehiclesNeeded = view.vehicles_needed_et
        orderButton = view.order_button

        orderButton.setOnClickListener {
            val order = Order(idEditText.text.toString().toInt(),itemEditText.text.toString(),
                    noOfItems.text.toString().toInt(),vehiclesNeeded.text.toString().toInt(),false,false)
            orderViewModel.placeOrder(order)
            clearFields()

        }
        return view
    }
    private fun clearFields(){
        idEditText.setText("")
        itemEditText.setText("")
        noOfItems.setText("")
        vehiclesNeeded.setText("")
    }


}
