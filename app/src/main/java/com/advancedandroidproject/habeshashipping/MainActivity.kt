package com.advancedandroidproject.habeshashipping

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.NavGraph
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupWithNavController
import com.advancedandroidproject.habeshashipping.data.Driver
import com.advancedandroidproject.habeshashipping.network.ShippingApiService
import com.advancedandroidproject.habeshashipping.repository.DriverRepository
import com.advancedandroidproject.habeshashipping.repository.OrderingCompanyRepository
import com.advancedandroidproject.habeshashipping.viewmodel.DriverViewModel
import com.advancedandroidproject.habeshashipping.viewmodel.ManagerViewModel
import com.advancedandroidproject.habeshashipping.viewmodel.OrderingCompanyViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var driverViewModel: DriverViewModel
    private lateinit var orderingCompanyViewModel: OrderingCompanyViewModel
    private lateinit var driverRepository: DriverRepository
    private lateinit var orderingCompanyRepository: OrderingCompanyRepository
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        driverViewModel = ViewModelProviders.of(this).get(DriverViewModel::class.java)
        orderingCompanyViewModel = ViewModelProviders.of(this).get(OrderingCompanyViewModel::class.java)
//
//        GlobalScope.launch(Dispatchers.IO) {
//            if (connected()) {
//              driverRepository.getAllDriversFromApi()
//              orderingCompanyRepository.getAllOrderingCompaniesFromApi()
//
//            }
//
//        }
//
//        //save  room data in to api
//        GlobalScope.launch(Dispatchers.IO) {
//            if (connected()) {
//                driverViewModel.allDrivers.observe(this@MainActivity,Observer{
//                    for(driver in it) {
//                        val response: Response<Void> =
//                            ShippingApiService.getInstance().registerDriver(driver).getCompleted()
//                    }
//
//                })
//
//            }
//

//        }


        val host: NavHostFragment = supportFragmentManager
                .findFragmentById(R.id.my_nav_host_fragment) as NavHostFragment? ?: return
        val navController = host.navController
        val driver = intent.getStringExtra("driver")
        val orderingCompany = intent.getStringExtra("orderingCompany")
        val manager = intent.getStringExtra("manager")

        if(driver == "Driver"){
            Toast.makeText(this,"This is Driver", Toast.LENGTH_SHORT).show()
            navController.setGraph(R.navigation.shipping_nav_driver)
            setupBottomNavMenu(navController,"driver")

        }
        else if(manager == "Manager"){
            Toast.makeText(this,"This is Manager", Toast.LENGTH_SHORT).show()
            navController.setGraph(R.navigation.shipping_nav)
            setupBottomNavMenu(navController,"manager")
        }
        else{
            Toast.makeText(this,"This is Ordering Company", Toast.LENGTH_SHORT).show()
            navController.setGraph(R.navigation.shipping_nav_company)
            setupBottomNavMenu(navController,"orderingCompany")
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(findNavController(R.id.my_nav_host_fragment))
                || super.onOptionsItemSelected(item)
    }
    private fun setupBottomNavMenu(navController: NavController,user: String) {
        val bottomNav = findViewById<BottomNavigationView>(R.id.bottom_nav_view)
        if(user == "driver") bottomNav.inflateMenu(R.menu.bottom_nav_menu_driver)
//        bottomNav.inflateMenu(R.menu.bottom_nav_menu)
        else if(user == "manager") bottomNav.inflateMenu(R.menu.bottom_nav_menu)
        else bottomNav.inflateMenu(R.menu.bottom_nav_menu_company)
        bottomNav?.setupWithNavController(navController)
    }


    private fun connected():Boolean {

        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo

        return networkInfo != null && networkInfo.isConnected

    }
}
