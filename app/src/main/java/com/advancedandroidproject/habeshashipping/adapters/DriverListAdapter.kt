package com.advancedandroidproject.habeshashipping.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.advancedandroidproject.habeshashipping.DriverListFragmentDirections
import com.advancedandroidproject.habeshashipping.R
import com.advancedandroidproject.habeshashipping.data.Driver
import kotlinx.android.synthetic.main.driver_recycler_view_item.view.*

class DriverListAdapter : RecyclerView.Adapter<DriverListAdapter.DriverListViewHolder>() {

    private var drivers :List<Driver> = emptyList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DriverListViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val recyclerViewItem = inflater.inflate(R.layout.driver_recycler_view_item,parent,false)
        recyclerViewItem.setOnClickListener {
            val id = it.driver_id_tv.text.toString().toInt()
            val driver = drivers.find{it.id == id} as Driver
//            val args = Bundle()
//            args.putSerializable("Driver",driver)
            val direction = DriverListFragmentDirections.driverDetail(driver.name)
            it.findNavController().navigate(direction)

        }
        return DriverListViewHolder(
            recyclerViewItem
        )
    }

    override fun getItemCount() = drivers.size

    override fun onBindViewHolder(holder: DriverListViewHolder, position: Int) {
        val driver = drivers[position]
        holder.driverId.text = driver.id.toString()
        holder.driverName.text = driver.name
    }
    fun setDrivers(drivers : List<Driver>){
        this.drivers = drivers
        notifyDataSetChanged()
    }

    class DriverListViewHolder(view : View) : RecyclerView.ViewHolder(view){
        val  driverId = view.driver_id_tv
        val  driverName = view.driver_name_tv
    }
}