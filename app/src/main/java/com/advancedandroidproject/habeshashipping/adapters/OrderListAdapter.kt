package com.advancedandroidproject.habeshashipping.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.advancedandroidproject.habeshashipping.OrderListFragmentDirections
import com.advancedandroidproject.habeshashipping.R
import com.advancedandroidproject.habeshashipping.data.Order
import kotlinx.android.synthetic.main.order_recycler_view_item.view.*

class OrderListAdapter: RecyclerView.Adapter<OrderListAdapter.OrderViewHolder>() {

    private var orders: List<Order> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val recyclerViewItem = inflater.inflate(R.layout.order_recycler_view_item,parent,false)
        recyclerViewItem.setOnClickListener{
            val id = it.order_id_tv.text.toString().toInt()
            val order = orders.find{it.id == id} as Order
//            val args = Bundle()
//            args.putSerializable("Order",order)
            val direction = OrderListFragmentDirections.orderDetail(order.item)
            it.findNavController().navigate(direction)

        }
        return OrderViewHolder(recyclerViewItem)
    }
    override fun getItemCount()= orders.size

    override fun onBindViewHolder(holder: OrderViewHolder, position: Int) {
        val order = orders[position]
        holder.orderId.text = order.id.toString()
        holder.orderItem.text = order.item
    }
    fun setOrders(orders : List<Order>){
        this.orders = orders
        notifyDataSetChanged()
    }


    class OrderViewHolder(view: View): RecyclerView.ViewHolder(view){
        val orderId = view.order_id_tv
        val orderItem = view.order_item_tv
    }
}