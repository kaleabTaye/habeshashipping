package com.advancedandroidproject.habeshashipping.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.advancedandroidproject.habeshashipping.AvailableDriversListFragment
import com.advancedandroidproject.habeshashipping.AvailableDriversListFragmentDirections
import com.advancedandroidproject.habeshashipping.R
import com.advancedandroidproject.habeshashipping.data.Driver
import com.advancedandroidproject.habeshashipping.viewmodel.DriverViewModel
import kotlinx.android.synthetic.main.available_drivers_item.view.*

class AvailableDriversListAdapter: RecyclerView.Adapter<AvailableDriversListAdapter.AvailableDriversViewHolder>() {

    private var availableDrivers: List<Driver> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AvailableDriversViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val recyclerViewItem = inflater.inflate(R.layout.available_drivers_item,parent,false)
        recyclerViewItem.setOnClickListener {
            val name = it.available_driver_tv.text.toString()
            val driver = availableDrivers.find{it.name == name} as Driver
//            val args = Bundle()
//            args.putSerializable("Driver",driver)
            val direction = AvailableDriversListFragmentDirections.taskAssignedToDriver(driver.name)
        //    val selectedDriver = Driver(driver.id,driver.name,driver.experience,false,driver.userName,driver.password)
            it.findNavController().navigate(direction)
        }
        return AvailableDriversViewHolder(recyclerViewItem)
    }

    override fun getItemCount()= availableDrivers.size

    override fun onBindViewHolder(holder: AvailableDriversViewHolder, position: Int) {
        val driver = availableDrivers[position]
        holder.driverName.text = driver.name
    }
    fun setAvailableDrivers(availableDrivers :List<Driver>){
        this.availableDrivers = availableDrivers
        notifyDataSetChanged()
    }



    class AvailableDriversViewHolder(view: View): RecyclerView.ViewHolder(view){
        val driverName = view.available_driver_tv
    }
}