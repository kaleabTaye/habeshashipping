package com.advancedandroidproject.habeshashipping


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.advancedandroidproject.habeshashipping.data.OrderingCompany
import com.advancedandroidproject.habeshashipping.viewmodel.OrderingCompanyViewModel
import kotlinx.android.synthetic.main.fragment_register_company.view.*


class RegisterCompanyFragment : Fragment() {

    private lateinit var nameEditText: EditText
    private lateinit var userNameEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var registerButton: Button
    private lateinit var idEditText: EditText

    private lateinit var orderingCompanyViewModel: OrderingCompanyViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        orderingCompanyViewModel = ViewModelProviders.of(this).get(OrderingCompanyViewModel::class.java)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_register_company, container, false)
        nameEditText = view.update_name_et
        userNameEditText = view.update_user_name_et
        passwordEditText = view.password_et
        registerButton = view.register_button_fromlogin
        idEditText = view.id_edit_text

        registerButton.setOnClickListener {
            val orderingCompany = OrderingCompany(idEditText.text.toString().toInt(),nameEditText.text.toString(),userNameEditText.text.toString(),
                passwordEditText.text.toString())
            orderingCompanyViewModel.registerCompany(orderingCompany)
            findNavController().navigate(R.id.loginFragment, null)
        }

        return view
    }


}
