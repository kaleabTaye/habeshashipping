package com.advancedandroidproject.habeshashipping


import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.findNavController
import com.advancedandroidproject.habeshashipping.data.Driver
import com.advancedandroidproject.habeshashipping.data.DriverDao
import com.advancedandroidproject.habeshashipping.data.Manager
import com.advancedandroidproject.habeshashipping.data.OrderingCompany
import com.advancedandroidproject.habeshashipping.repository.DriverRepository
import com.advancedandroidproject.habeshashipping.viewmodel.DriverViewModel
import com.advancedandroidproject.habeshashipping.viewmodel.ManagerViewModel
import com.advancedandroidproject.habeshashipping.viewmodel.OrderingCompanyViewModel
import kotlinx.android.synthetic.main.fragment_login.view.*
import androidx.databinding.ViewDataBinding


class LoginFragment : Fragment() {


    private lateinit var userNameEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var loginButton: Button
    private lateinit var registerButton: Button
    private lateinit var driverDao: DriverDao

    private lateinit var driverViewModel: DriverViewModel
    private lateinit var managerViewModel: ManagerViewModel
    private lateinit var orderingCompanyViewModel: OrderingCompanyViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        driverViewModel = ViewModelProviders.of(this).get(DriverViewModel::class.java)
        managerViewModel = ViewModelProviders.of(this).get(ManagerViewModel::class.java)
        orderingCompanyViewModel = ViewModelProviders.of(this).get(OrderingCompanyViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_login, container, false)
//        val binding = DataBindingUtil.inflate<ViewDataBinding>(
//            inflater, R.layout.fragment_login, container, false
//        ).apply{
//
//        }
//        val view = binding.getRoot()
        userNameEditText = view.username_edit_text
        passwordEditText = view.password_edit_text
        loginButton = view.login_button
        registerButton = view.register_button_fromlogin
        var driver: Driver
        var manager: Manager
        var orderingCompany: OrderingCompany

        registerButton.setOnClickListener {
            findNavController().navigate(R.id.registerCompanyFragment, null)
        }

        loginButton.setOnClickListener {
            AsyncTask.execute {
                driver = driverViewModel.validateDriver(userNameEditText.text.toString(), passwordEditText.text.toString())
                manager = managerViewModel.validateManager(userNameEditText.text.toString(), passwordEditText.text.toString())
                orderingCompany = orderingCompanyViewModel.validateOrderingCompany(userNameEditText.text.toString(), passwordEditText.text.toString()
                )
                val intent = Intent(activity, MainActivity::class.java)
                if (manager != null) {
                    val args = Bundle()
                    //args.putSerializable("Manager",manager)
                    val managerHomeFragment = ManagerHomeFragment()
                    managerHomeFragment.arguments = args
                    intent.putExtra("manager", "Manager")
                    startActivity(intent)
                } else if (orderingCompany != null) {
                    val args = Bundle()
                    //args.putSerializable("OrderingCompany", orderingCompany)
                    val orderingCompanyHomeFragment = OrderingCompanyHomeFragment()
                    orderingCompanyHomeFragment.arguments = args
                    intent.putExtra("orderingCompany", "OrderingCompany")
                    startActivity(intent)
                } else if (driver != null) {
                    val args = Bundle()
                    //args.putSerializable("Driver",driver)
                    val driverHomeFragment = DriverHomeFragment()
                    driverHomeFragment.arguments = args
                    intent.putExtra("driver", "Driver")
                    startActivity(intent)
                }
            }

        }
        return view
    }

}

