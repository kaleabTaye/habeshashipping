package com.advancedandroidproject.habeshashipping


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.advancedandroidproject.habeshashipping.adapters.AvailableDriversListAdapter
import com.advancedandroidproject.habeshashipping.data.Driver
import com.advancedandroidproject.habeshashipping.data.Order
import com.advancedandroidproject.habeshashipping.viewmodel.DriverViewModel
import com.advancedandroidproject.habeshashipping.viewmodel.OrderViewModel
import kotlinx.android.synthetic.main.fragment_available_drivers_list.view.*

class AvailableDriversListFragment : Fragment() {

    private lateinit var driverList: RecyclerView
    private lateinit var driverViewModel: DriverViewModel
    private lateinit var orderViewModel: OrderViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        driverViewModel = ViewModelProviders.of(this).get(DriverViewModel::class.java)
        orderViewModel = ViewModelProviders.of(this).get(OrderViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view= inflater.inflate(R.layout.fragment_available_drivers_list, container, false)
        driverList = view.available_driver_list
        driverList.layoutManager = LinearLayoutManager(activity)
        val driverAdapter = AvailableDriversListAdapter()
        driverList.adapter = driverAdapter
        driverList.setHasFixedSize(true)
        driverViewModel.getAvailableDrivers().observe(this, Observer{
                availableDrivers -> availableDrivers?.let{driverAdapter.setAvailableDrivers(availableDrivers)}
        })
//        driverViewModel.allDrivers.observe(this, Observer{
//                val driver:ArrayList<Driver> = ArrayList()
//            it.forEach {if(it.isAvailable == true){ driver.add(it)}  }
//            driverAdapter.setAvailableDrivers(driver)
//        })

        val item = arguments?.getString("orderToDriverArgs") as String
        orderViewModel.searchItem(item).observe(this,Observer{
            val order = Order(it.id,it.item,it.numOfItems,it.vechilesNeeded,true,false)
            orderViewModel.editOrder(order)
        })

        return view
    }


}
