package com.advancedandroidproject.habeshashipping


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.advancedandroidproject.habeshashipping.adapters.DriverListAdapter
import com.advancedandroidproject.habeshashipping.viewmodel.DriverViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.fragment_driver_list.view.*


class DriverListFragment : Fragment() {

    private lateinit var driverList: RecyclerView
    private lateinit var driverViewModel: DriverViewModel
    private lateinit var driverAddFab: FloatingActionButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        driverViewModel = ViewModelProviders.of(this).get(DriverViewModel::class.java)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view =  inflater.inflate(R.layout.fragment_driver_list, container, false)
        driverAddFab = view.driver_add_fab
        driverAddFab.setOnClickListener {
            Toast.makeText(activity,"HEllo",LENGTH_SHORT).show()
            findNavController().navigate(R.id.registerDriverFragment, null)
        }
        driverList = view.driver_list
        driverList.layoutManager = LinearLayoutManager(activity)
        val driverAdapter = DriverListAdapter()
        driverList.adapter = driverAdapter
        driverList.setHasFixedSize(true)
        driverViewModel.allDrivers.observe(this, Observer{
                drivers -> drivers?.let{driverAdapter.setDrivers(drivers)}
        })


        return view
    }


}
