package com.advancedandroidproject.habeshashipping.repository

import androidx.lifecycle.LiveData
import androidx.room.Query
import com.advancedandroidproject.habeshashipping.data.Driver
import com.advancedandroidproject.habeshashipping.data.DriverDao
import com.advancedandroidproject.habeshashipping.network.ShippingApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class DriverRepository(private val driverDao:DriverDao) {


    suspend fun getAllDriversFromApi(){
        withContext(Dispatchers.IO) {
            val response: Response<List<Driver>> =
                ShippingApiService.getInstance().listAllDrivers().await()
            if (response.isSuccessful) {
                val drivers = response.body() as List<Driver>
                for (driver in drivers) {
                    driverDao.registerDriver(driver)
                }
            }
        }
    }

    suspend fun registerDriverToApi(driver:Driver){
        withContext(Dispatchers.IO){
            val response: Response<Void> =  ShippingApiService.getInstance().registerDriver(driver).await()
            registerDriver(driver)
        }
    }

    suspend fun updateDriverToApi(id: Int,driver: Driver){
        withContext(Dispatchers.IO){
            val response: Response<Void> = ShippingApiService.getInstance().updateDriver(id,driver).await()
            updateDriver(driver)
        }
    }
    suspend fun deleteDriverFromApi(id: Int,driver: Driver){
        withContext(Dispatchers.IO){
            val response: Response<Void> = ShippingApiService.getInstance().deleteDriver(id).await()
            deleteDriver(driver)
        }
    }


    fun getAllDriversFromRoom()  = driverDao.getAllDrivers()

    fun searchDriver(name:String) = driverDao.searchDriver(name)

    fun validateDriver(userName:String,password:String)  = driverDao.validateDriver(userName,password)

    fun getAvailableDrivers() = driverDao.getAvailableDrivers()

    fun registerDriver(driver:Driver){
        driverDao.registerDriver(driver)
    }
    fun deleteDriver(driver:Driver){
        driverDao.deleteDriver(driver)
    }
    fun updateDriver(driver:Driver){
        driverDao.updateDriver(driver)
    }
}