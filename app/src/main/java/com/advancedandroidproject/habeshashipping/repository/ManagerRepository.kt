package com.advancedandroidproject.habeshashipping.repository

import androidx.lifecycle.LiveData
import com.advancedandroidproject.habeshashipping.data.Manager
import com.advancedandroidproject.habeshashipping.data.ManagerDao
import com.advancedandroidproject.habeshashipping.network.ShippingApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class ManagerRepository(private val managerDao: ManagerDao) {

    suspend fun getManagerFromApi(){
        withContext(Dispatchers.IO){
            val response: Response<Manager> =
                ShippingApiService.getInstance().getManager().await()
            if (response.isSuccessful) {
                val manager = response.body() as Manager
                managerDao.insertManager(manager)

            }
        }
    }

    suspend fun updateManagerToApi(id: Int,manager: Manager){
        withContext(Dispatchers.IO){
            val response: Response<Void> =
                ShippingApiService.getInstance().updateManager(id,manager).await()

        }
    }

    fun insertManager(manager: Manager) = managerDao.insertManager(manager)

    fun validateManager(userName:String,password: String) = managerDao.validateManager(userName,password)

    fun updateManager(manager: Manager){
        managerDao.updateManager(manager)
    }

}