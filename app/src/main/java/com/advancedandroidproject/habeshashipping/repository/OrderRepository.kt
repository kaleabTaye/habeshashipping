package com.advancedandroidproject.habeshashipping.repository

import androidx.lifecycle.LiveData
import com.advancedandroidproject.habeshashipping.data.Order
import com.advancedandroidproject.habeshashipping.data.OrderDao
import com.advancedandroidproject.habeshashipping.network.ShippingApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class OrderRepository(private val orderDao: OrderDao) {

    suspend fun getAllOrdersFromApi(){
        withContext(Dispatchers.IO){
            val response: Response<List<Order>> =
                ShippingApiService.getInstance().getOrders().await()
            if(response.isSuccessful){
                val orders = response.body() as List<Order>
                for(order in orders){
                    orderDao.placeOrder(order)
                }
            }
        }
    }

    suspend fun registerOrderToApi(order: Order){
        withContext(Dispatchers.IO){
            val response: Response<Void> =
                ShippingApiService.getInstance().placeOrder(order).await()
                placeOrder(order)
        }
    }

    suspend fun updateOrderToApi(id: Int, order:Order){
        withContext(Dispatchers.IO){
            val response: Response<Void> =
                ShippingApiService.getInstance().updateOrder(id,order).await()
                editOrder(order)
        }
    }

    suspend fun deleteOrderFromApi(id: Int,order: Order){
        withContext(Dispatchers.IO){
            val response: Response<Void> =
                ShippingApiService.getInstance().deleteOrder(id).await()
                deleteOrder(order)
        }
    }

    fun searchItem(item: String)= orderDao.searchItem(item)

    fun getAllItems() = orderDao.getAllItems()

    fun placeOrder(order: Order){
        orderDao.placeOrder(order)
    }
    fun editOrder(order: Order){
        orderDao.editOrder(order)
    }
    fun deleteOrder(order: Order){
        orderDao.deleteOrder(order)
    }
}