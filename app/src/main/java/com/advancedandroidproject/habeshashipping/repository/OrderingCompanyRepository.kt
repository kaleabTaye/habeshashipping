package com.advancedandroidproject.habeshashipping.repository

import androidx.lifecycle.LiveData
import com.advancedandroidproject.habeshashipping.data.OrderingCompany
import com.advancedandroidproject.habeshashipping.data.OrderingCompanyDao
import com.advancedandroidproject.habeshashipping.network.ShippingApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class OrderingCompanyRepository(private val orderingCompanyDao: OrderingCompanyDao) {

    suspend fun getAllOrderingCompaniesFromApi(){
        withContext(Dispatchers.IO){
            val response: Response<List<OrderingCompany>> =
                ShippingApiService.getInstance().getOrderingCompanies().await()
            if(response.isSuccessful){
                val orderingCompany = response.body() as List<OrderingCompany>
                for(company in orderingCompany){
                    orderingCompanyDao.registerCompany(company)
                }
            }
        }
    }

    suspend fun registerOrderingCompanyToApi(orderingCompany: OrderingCompany){
        withContext(Dispatchers.IO){
            val response: Response<Void> = ShippingApiService.getInstance().registerOrderingCompany(orderingCompany).await()
            registerCompany(orderingCompany)
        }
    }

    suspend fun updateOrderingCompanyToApi(id: Int,orderingCompany: OrderingCompany){
        withContext(Dispatchers.IO){
            val response: Response<Void> = ShippingApiService.getInstance().updateOrderingCompany(id,orderingCompany).await()
            updateCompany(orderingCompany)
        }
    }

    fun registerCompany(orderingCompany: OrderingCompany){
        orderingCompanyDao.registerCompany(orderingCompany)
    }
    fun updateCompany(orderingCompany: OrderingCompany){
        orderingCompanyDao.updateCompany(orderingCompany)
    }
    fun validateOrderingCompany(userName:String,password:String)= orderingCompanyDao.validateOrderingCompany(userName,password)

}